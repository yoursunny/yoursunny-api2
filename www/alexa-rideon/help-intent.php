<?php
speak(
  'I can help you check bus schedule using a five-digit stop number. '.
  'You may find stop numbers on ride on real time dot com under route schedules section. '.
  'Check your Alexa app for a link. '.
  'What\'s your inquiry?'
);
$resp['card'] = array(
  'type'=>'Standard',
  'title'=>'Find Ride On stop number',
  'text'=>'To find the five-digit stop number of a Ride On bus stop, navigate to https://rideonrealtime.com/Schedule.aspx , and select your route and stop.',
);
keepSessionOpen();
?>
