<?php
$id = $intent->slots->id_a->value . $intent->slots->id_b->value;
if (preg_match('/^[0-9]{5}$/', $id) !== 1) {
  speak('Sorry, I can\'t recognize the five-digit stop number.');
  return;
}

$db = new SQLite3('RideOnGTFS.sqlite3');
define('GTFSSQL_SERVICES_BY_DATE', 'SELECT service_id_map.service_id_str AS service_id FROM service_id_map INNER JOIN calendar ON service_id_map.service_id=calendar.service_id WHERE calendar.date=:date');
$stmt = $db->prepare('SELECT stop_name,route_number,trip_headsign,departure_time FROM routes INNER JOIN trips ON routes.route_id=trips.route_id INNER JOIN stop_times ON trips.trip_id=stop_times.trip_id INNER JOIN stops ON stop_times.stop_id=stops.stop_id WHERE service_id IN ('.GTFSSQL_SERVICES_BY_DATE.') AND stop_code=:stopid AND departure_time>=:time ORDER BY departure_time LIMIT 3');
$stmt->bindValue(':date', date('Ymd'));
$stmt->bindValue(':time', date('H:i'));
$stmt->bindValue(':stopid', $id);
$result = $stmt->execute();

$msg = '';
while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
  if ($msg == '') {
    $msg = sprintf('The next departures from %s are: ', $row['stop_name']);
  }
  $msg.= sprintf('route %s to %s at %s, ', $row['route_number'], $row['trip_headsign'], substr($row['departure_time'], 0, 5));
}
$result->finalize();

if ($msg == '') {
  $msg = 'Sorry, I can\'t find this stop number.';
}

speak($msg);
?>
