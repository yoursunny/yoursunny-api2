<?php
date_default_timezone_set('America/New_York');
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

use Alexa\Utility\Purifier\PurifierFactory;

class AlexaCertificate extends \Alexa\Request\Certificate
{
  protected function validateTimestamp($timestamp)
  {
    // Generate DateTimes
    $currentDateTime = new \DateTime();
    $timestamp = new \DateTime((is_int($timestamp) ? '@' : '').$timestamp);

    // Compare
    $differenceInSeconds = $currentDateTime->getTimestamp() - $timestamp->getTimestamp();
    if ($differenceInSeconds > self::TIMESTAMP_VALID_TOLERANCE_SECONDS) {
        throw new \InvalidArgumentException(self::ERROR_REQUEST_EXPIRED);
    }
  }
}

$cert = new AlexaCertificate($_SERVER['HTTP_SIGNATURECERTCHAINURL'], $_SERVER['HTTP_SIGNATURE'], PurifierFactory::generatePurifier(PurifierFactory::DEFAULT_CACHE_PATH));

$rawJson = file_get_contents('php://input');
$cert->validateRequest($rawJson);
$request = json_decode($rawJson);

if ($request->session->application->applicationId != 'amzn1.ask.skill.60aded80-012b-4c2f-91fb-0461238770c2') {
  die('bad skill id');
}
$req = $request->request;
$resp = array();
$resp['shouldEndSession'] = TRUE;

function speak($text) {
  global $resp;
  $resp['outputSpeech'] = array(
    'type'=>'PlainText',
    'text'=>$text,
  );
}

function keepSessionOpen() {
  global $resp;
  $resp['shouldEndSession'] = FALSE;
}

switch ($req->type) {
  case 'SessionEndedRequest':
    break;
  case 'LaunchRequest':
    include 'launch.php';
    break;
  case 'IntentRequest':
    $intent = $req->intent;
    switch ($intent->name) {
      case 'AMAZON.StopIntent':
      case 'AMAZON.CancelIntent':
        include 'stop-intent.php';
        break;
      case 'AMAZON.HelpIntent':
        include 'help-intent.php';
        break;
      case 'ScheduleIntent':
        include 'schedule-intent.php';
        break;
    }
    break;
}

header('Content-Type: application/json; charset=UTF-8');
echo json_encode(array('version'=>'1.0', 'response'=>$resp));
?>
