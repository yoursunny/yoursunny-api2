<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

use Alexa\Utility\Purifier\PurifierFactory;

class AlexaCertificate extends \Alexa\Request\Certificate
{
  protected function validateTimestamp($timestamp)
  {
    // Generate DateTimes
    $currentDateTime = new \DateTime();
    $timestamp = new \DateTime((is_int($timestamp) ? '@' : '').$timestamp);

    // Compare
    $differenceInSeconds = $currentDateTime->getTimestamp() - $timestamp->getTimestamp();
    if ($differenceInSeconds > self::TIMESTAMP_VALID_TOLERANCE_SECONDS) {
        throw new \InvalidArgumentException(self::ERROR_REQUEST_EXPIRED);
    }
  }
}

$rawJson = file_get_contents('php://input');
$cert = new AlexaCertificate($_SERVER['HTTP_SIGNATURECERTCHAINURL'], $_SERVER['HTTP_SIGNATURE'], PurifierFactory::generatePurifier(PurifierFactory::DEFAULT_CACHE_PATH));
$cert->validateRequest($rawJson);

header('Content-type: application/json');
$ch = curl_init('https://triggers.losant.com/webhooks/PkX3jaaxbrUydZMAPwDoUD3qnYQ');
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $rawJson);
curl_exec($ch);
?>
