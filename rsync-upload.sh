#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"

rsync -rLpvog --delete --delete-excluded --chmod=D770,F660 --chown=sunny:www-data \
  ./ vps4:/home/web/api2
