# api2.yoursunny.cn

## Installation

1. Setup site on Cloudflare.
2. Obtain Cloudflare Origin certificate, save to `cloudflare-origin-public.pem` and `cloudflare-origin-private.pem`.
3. `composer install`

## Development

1. Download [ngrok](https://ngrok.com) binary to `~/.ngrok/ngrok`.
2. `./devserver.sh`
